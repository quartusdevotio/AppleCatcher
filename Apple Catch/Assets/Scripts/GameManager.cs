using UnityEngine;
using System.Collections;
using AppleCatch.Player;
using AppleCatch.Spawner;
using AppleCatch.UI;

namespace AppleCatch.Manager
{
    public class GameManager : MonoBehaviour
    {
        private PlayerController playerController;
        private PlayerCollisionChecker playerCollisionChecker;
        [SerializeField] private ItemSpawner itemSpawner;
        [SerializeField] private LifeManager lifeManager;
        [SerializeField] private UIController uiController;
        [SerializeField] private ScoreManager scoreManager;

        private Coroutine movePlatformCoroutine;
        [SerializeField] private Transform platformsTransform;
        [SerializeField] private Vector2[] targetPositions;
        [SerializeField] private float speed = 1f;

        [SerializeField] private GameObject appleSpawner;
        [SerializeField] private GameObject backgroundFlame;
        [SerializeField] private GameObject gameOverCanvas;

        // Start is called before the first frame update
        void Start()
        {
            Initialize();
        }

        // Update is called once per frame
        void Update()
        {
            playerController.SetOnGround(playerCollisionChecker.CheckGround());
        }

        private void Initialize()
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            playerController = player.GetComponent<PlayerController>();
            playerCollisionChecker = player.GetComponent<PlayerCollisionChecker>();

            playerController.PlayerDead += OnPlayerDead;
            scoreManager.ScoreChanged += uiController.OnScoreChanged;
            lifeManager.PlayerTakeDamage += OnPlayerTakeDamage;
            itemSpawner.ScoreAdded += scoreManager.AddScore;
            itemSpawner.AppleMissed += lifeManager.SubtractLife;
        }

        private void OnPlayerDead()
        {
            if(movePlatformCoroutine != null)
            {
                StopCoroutine(movePlatformCoroutine);
            }
            Destroy(appleSpawner);
            Destroy(backgroundFlame);
            Destroy(platformsTransform.gameObject);
            lifeManager.DisableCollider();
            gameOverCanvas.SetActive(true);
        }

        private void OnPlayerTakeDamage()
        {
            if(playerController.isDead)
            {
                return;
            }
            movePlatformCoroutine = StartCoroutine(MovePlatformDown());
        }

        private IEnumerator MovePlatformDown()
        {
            while(platformsTransform.transform.position.y > targetPositions[lifeManager.Life].y)
            {
                float step = speed * Time.deltaTime;
                platformsTransform.transform.position = Vector2.MoveTowards(platformsTransform.transform.position, targetPositions[lifeManager.Life], step);
                yield return new WaitForSeconds(step);
            }
            yield return null;
        }
    }
}