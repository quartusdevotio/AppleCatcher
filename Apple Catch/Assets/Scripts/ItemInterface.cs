using System;

namespace AppleCatch.Interface
{
    public interface INormalObject
    {
        public event Action<int> ScoreAdded;
        void AddScore();
        
        public event Action<int> AppleMissed;
        void SubtractLife();
    }
}