using System;
using UnityEngine;

namespace AppleCatch.Manager
{
    public class LifeManager : MonoBehaviour
    {
        private int life = 3;
        public int Life 
        {
            get { return life; }
        }

        public event Action PlayerTakeDamage;

        private EdgeCollider2D destroyerCollider2D;

        private void Start()
        {
            destroyerCollider2D = GetComponent<EdgeCollider2D>();
        }

        public void SubtractLife(int damage)
        {
            if (life == 0)
            {
                return;
            }

            life -= damage;
            PlayerTakeDamage?.Invoke();
        }

        public void DisableCollider()
        {
            destroyerCollider2D.enabled = false;
        }
    }
}