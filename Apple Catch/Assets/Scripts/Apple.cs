using AppleCatch.Manager;
using AppleCatch.Interface;
using UnityEngine;
using System;

namespace AppleCatch.Objects
{
    public class Apple : Item, INormalObject
    {
        public event Action<int> ScoreAdded;
        public event Action<int> AppleMissed;

        private void Start()
        {
            rigidBody2D = GetComponent<Rigidbody2D>();
        }

        private void OnDestroy()
        {
            ScoreAdded = null;
            AppleMissed = null;
        }

        public void AddScore()
        {
            ScoreAdded?.Invoke(1);
            AudioManager.instance.PlaySfx("PickUp", 1f);
            Destroy(gameObject);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                AddScore();
            }
            else if (collision.CompareTag("ItemDestroyer"))
            {
                Destroy(gameObject);
            }
        }

        public void SubtractLife()
        {
            
        }
    }
}
