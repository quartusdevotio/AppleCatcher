using System.Collections;
using System;
using UnityEngine;
using AppleCatch.Manager;
using System.Collections.Generic;
using AppleCatch.Objects;
using AppleCatch.Interface;

namespace AppleCatch.Spawner
{
    public class ItemSpawner : MonoBehaviour
    {
        private static float YPos = 4.5f;
        [SerializeField] private GameObject[] itemPrefab;
        [SerializeField] private float minimumOffset;
        [SerializeField] private float maximumOffset;
        [SerializeField] private float cooldown = 1f;
        [SerializeField] private bool stop;

        [SerializeField] private float[] percentages;
        public List<Item> apples = new List<Item>();
        public event Action<int> ScoreAdded;
        public event Action<int> AppleMissed;

        void Start()
        {
            StartCoroutine(Spawn());
        }

        private void OnDestroy()
        {
            ScoreAdded = null;
            AppleMissed = null;
        }

        IEnumerator Spawn()
        {
            while (!stop)
            {
                Vector2 randomPos = new Vector2(UnityEngine.Random.Range(minimumOffset, maximumOffset), YPos);
                INormalObject tempApple = Instantiate(itemPrefab[RandomSpawn()], randomPos, Quaternion.identity).GetComponent<INormalObject>();
                tempApple.ScoreAdded += OnScoreAdded;
                tempApple.AppleMissed += OnAppleMissed;
                AudioManager.instance.PlaySfx("AppleSpawn", 0.3f);
                yield return new WaitForSeconds(cooldown);
            }
        }

        private void OnAppleMissed(int score)
        {
            AppleMissed?.Invoke(score);
        }

        private void OnScoreAdded(int score)
        {
            ScoreAdded?.Invoke(score);
        }

        private int RandomSpawn()
        {
            int output = 0;
            float random = UnityEngine.Random.Range(0f, 1f);
            float total = 0f;
            float addNum = 0f;

            for (int i = 0; i < percentages.Length; i++)
            {
                total += percentages[i];
            }

            for (int i = 0; i < itemPrefab.Length; i++)
            {
                if (percentages[i] / total + addNum >= random)
                {
                    output = i;
                    return output;
                }
                else
                {
                    addNum += percentages[i] / total;
                }
            }
            return 0;
        }
    }
}