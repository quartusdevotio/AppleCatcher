using AppleCatch.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace AppleCatch.UI
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private Slider musicSlider;
        [SerializeField] private TMP_Text scoreText;

        public void ChangeMusicVolumeUBEvent()
        {
            AudioManager.instance.SetMusicVolume(musicSlider.value);
        }

        public void ReloadSceneUBEvent()
        {
            var activeScene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(activeScene);
            AudioManager.instance.PlayMusic("Omurice");
        }

        public void OnScoreChanged(int score)
        {
            scoreText.text = "Score: " + score.ToString();
        }
    }
}