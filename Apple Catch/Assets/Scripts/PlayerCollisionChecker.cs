using System;
using UnityEngine;

namespace AppleCatch.Player
{
    public class PlayerCollisionChecker : MonoBehaviour
    {
        private bool onGround;
        [SerializeField] private LayerMask groundLayer;
        [SerializeField] private Vector2 bottomOffset;
        [SerializeField] private float checkerRadius = 0.25f;

        void Update()
        {
            onGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, checkerRadius, groundLayer);
        }

        public bool CheckGround()
        {
            bool output = onGround;
            return output;
        }
    }
}